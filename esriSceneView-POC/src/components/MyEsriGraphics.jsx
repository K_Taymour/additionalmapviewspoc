import { useState, useEffect } from 'react';
import { loadModules } from 'esri-loader';
import missedCallPng from '../assets/icons/missed-call.png';
import ringingCallPng from '../assets/icons/ringing-call.png';
import './MyEsriGraphics.css';

const MyEsriGraphics = (props) => {

    const [graphic, setGraphic] = useState(null);
    useEffect(() => {

        loadModules(['esri/layers/GraphicsLayer', 'esri/Graphic', "esri/layers/support/LabelClass", "esri/layers/GeoJSONLayer", "esri/symbols/TextSymbol", "esri/symbols/TextSymbol3DLayer"]).then(([GraphicsLayer, Graphic, LabelClass, GeoJSONLayer, TextSymbol, TextSymbol3DLayer]) => {
            // Create a polygon geometry
            const polygon = {
                type: "polygon", // autocasts as new Polygon()
                rings: [
                    [-0.184, 50.48391],
                    [-0.184, 52.49091],
                    [-2.172, 52.49091],
                    [-2.172, 50.48391],
                    [-0.184, 50.48391]
                ]
            };

            // Create a symbol for rendering the graphic
            const fillSymbol = {
                type: "simple-fill", // autocasts as new SimpleFillSymbol()
                color: [227, 139, 79, 0.8],
                outline: { // autocasts as new SimpleLineSymbol()
                    color: [255, 255, 255],
                    width: 1
                }
            };

            // Add the geometry and symbol to a new graphic
            const polygonGraphic = new Graphic({
                geometry: polygon,
                symbol: fillSymbol
            });

            /****************************
             * Add a 3D polyline graphic
             ****************************/
            const polyline = {
                type: "polyline", // autocasts as new Polyline()
                paths: [
                    [-0.178, 51.48791],
                    [-2.178, 53.48791]
                ]
            };

            const lineSymbol = {
                type: "simple-line", // autocasts as SimpleLineSymbol()
                color: [226, 119, 40],
                width: 4
            };

            const polylineGraphic = new Graphic({
                geometry: polyline,
                symbol: lineSymbol
            });

            /*************************
             * Add a 3D point graphic
             *************************/

            // London
            const londonPoint = {
                type: "point", // autocasts as new Point()
                x: -0.178,
                y: 51.48791,
            };

            //Manchester
            const manchesterPoint = {
                type: "point",
                x: -2.178,
                y: 53.48791
            }

            //amsterdam
            const amsterdamPoint = {
                type: "point",
                x: 4.9,
                y: 52.36
            }

            //Paris
            const parisPoint = {
                type: "point",
                x: 2.35,
                y: 48.8566
            }

            // const markerSymbol = {
            //     type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
            //     color: [226, 119, 40],
            //     outline: {
            //         // autocasts as new SimpleLineSymbol()
            //         color: [255, 255, 255],
            //         width: 2
            //     }
            // };

            let labelClass = new LabelClass({
                symbol: {
                    type: "label-3d", // autocasts as new LabelSymbol3D()
                    symbolLayers: [
                        {
                            type: "text", // autocasts as new TextSymbol3DLayer()
                            material: {
                                color: "white"
                            },
                            size: 10
                        }
                    ]
                },
                labelPlacement: "above-center",
                labelExpressionInfo: {
                    expression: 'it is a label'
                }
            });

            let picSymbol = {
                type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                url: missedCallPng,
                width: "40px",
                height: "40px"
            };

            let manchesterPicSymbol = {
                type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                url: ringingCallPng,
                width: "40px",
                height: "40px",
                declaredClass: 'redCircle'
            };

            let amsterdamTextSymbol = {
                type: "text",  // autocasts as new TextSymbol()
                color: "red",
                backgroundColor: "black",
                haloColor: "black",
                haloSize: "1px",
                text: "I am \nTextSymbol",
                xoffset: 0,
                yoffset: 0,
                font: {  // autocasts as new Font()
                    size: 12,
                    family: "Josefin Slab",
                    weight: "bold"
                }
            };

            let parisInnerSymbol = {
                type: "simple-marker", // autocasts as new SimpleMarkerSymbol() 
                outline: { // autocasts as new SimpleLineSymbol()
                    color: [206, 1, 1, 1]
                },
                size: 40,
                color: [255, 34, 34, 0]
            };

            let parisOuterSymbol = {
                type: "simple-marker", // autocasts as new SimpleMarkerSymbol() 
                outline: { // autocasts as new SimpleLineSymbol()
                    color: [206, 1, 1, 1]
                },
                size: 45,
                color: [255, 34, 34, 0]
            };

            let manchester3DTextSymbol = {
                type: "label-3d",  // autocasts as new LabelSymbol3D()
                symbolLayers: [{
                    type: "3d text",  // autocasts as new TextSymbol3DLayer()
                    material: { color: [49, 163, 84] },
                    size: 12  // points
                }]
            }

            // Configures clustering on the layer. A cluster radius
            // of 100px indicates an area comprising screen space 100px
            // in length from the center of the cluster
            const clusterConfig = {
                type: "cluster",
                clusterRadius: "100px",
                // {cluster_count} is an aggregate field containing
                // the number of features comprised by the cluster
                popupTemplate: {
                    content: "This cluster represents {cluster_count} earthquakes."
                }
            };


            // const clusterConfig = {
            //     type: "cluster",
            //     clusterRadius: "2000px",

            //     clusterMinSize: "2000px",
            //     // clusterMaxSize: "60px",
            // };

            const geojsonLayer = new GeoJSONLayer({
                title: "Earthquakes from the last month",
                url: "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.geojson",
                copyright: "USGS Earthquakes",
                featureReduction: clusterConfig,
                // popupTemplates can still be viewed on
                // individual features
                popupTemplate: {
                    title: "Earthquake Info",
                    content: "Magnitude {mag} {type} hit {place} on {time}"
                },
                renderer: {
                    type: "simple",
                    field: "mag",
                    symbol: {
                        type: "simple-marker",
                        size: 4,
                        color: "#fc3232",
                        outline: {
                            color: [50, 50, 50]
                        }
                    }
                }
            });


            const londonPointGraphic = new Graphic({
                geometry: londonPoint,
                symbol: picSymbol
            });

            const manchesterPointGraphic = new Graphic({
                geometry: manchesterPoint,
                symbol: manchesterPicSymbol
            });

            const amsterdamPointGraphic = new Graphic({
                geometry: amsterdamPoint,
                symbol: amsterdamTextSymbol
            });

            const parisInnerPointGraphic = new Graphic({
                geometry: parisPoint,
                symbol: parisInnerSymbol
            });

            const parisOuterPointGraphic = new Graphic({
                geometry: parisPoint,
                symbol: parisOuterSymbol
            });

            //#region Popup

            let template = {
                // NAME and COUNTY are fields in the service containing the Census Tract (NAME) and county of the feature
                title: "A Popup",
                content: `
                <div>div1</div>
                <div>div2</div>
                `
            };
            polygonGraphic.popupTemplate = template;
            polylineGraphic.popupTemplate = template;
            londonPointGraphic.popupTemplate = template;
            manchesterPointGraphic.popupTemplate = template;

            manchesterPointGraphic.labelingInfo = labelClass;
            //#endregion




            // Add graphic when GraphicsLayer is constructed
            let layer = new GraphicsLayer({
                graphics: [londonPointGraphic, manchesterPointGraphic],
                featureReduction: clusterConfig
            });
            // Add graphic to graphics collection
            // layer.graphics.add(manchesterPointGraphic);

            // setGraphic(polygonGraphic);
            // setGraphic(polylineGraphic);
            // setGraphic(layer);
            // setGraphic(londonPointGraphic);
            // setGraphic(manchesterPointGraphic);


            props.view.graphics.add(polygonGraphic);
            props.view.graphics.add(polylineGraphic);
            // props.view.graphics.add(layer);
            props.view.graphics.add(londonPointGraphic);
            props.view.graphics.add(manchesterPointGraphic);
            props.view.graphics.add(amsterdamPointGraphic);
            props.view.graphics.add(parisInnerPointGraphic);
            props.view.graphics.add(parisOuterPointGraphic);
            props.map.add(geojsonLayer);
        }).catch((err) => console.error(err));

        return function cleanup() {
            props.view.graphics.remove(graphic);
        };
    }, []);

    return null;

}

export default MyEsriGraphics;