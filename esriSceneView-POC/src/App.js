import React from 'react';
import logo from './logo.svg';
import './App.css';
import EsriSceneView from './components/esriSceneView';

function App() {
  return (
    <div className="App">
      <EsriSceneView />
    </div>
  );
}

export default App;
